package ooss;

import java.util.ArrayList;
import java.util.List;

public class Klass {
    private final Integer number;
    private Student klassLeader;
    private List<Person> klassMemberList = new ArrayList<>();

    public Klass(int number) {
        this.number = number;
    }

    public int getNumber() {
        return this.number;
    }

    private void printSpeakOfKlassAttachMember() {
        String commonSpeak = String.format(" I know %s become Leader.", this.klassLeader.name);

        for (Person klassMember: klassMemberList) {
            String character = klassMember instanceof Teacher ? "teacher" : "student";
            System.out.printf(
                    "I am %s, %s of Class %d." + commonSpeak,
                    klassMember.name, character, this.number
            );
        }
    }

    public void assignLeader(Student student) {
        if (this.equals(student.getKlass())) {
            this.klassLeader = student;
            printSpeakOfKlassAttachMember();
        } else {
            System.out.println("It is not one of us.");
        }
    }

    public Boolean isLeader(Student student) {
        return this.klassLeader == student;
    }

    public void attach(Person klassMember) {
        this.klassMemberList.add(klassMember);
    }


    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }

        if (!(o instanceof Klass)) {
            return false;
        }

        Klass anotherKlass = (Klass) o;
        return this.number.equals(anotherKlass.number);
    }
}
