package ooss;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Teacher extends Person {

    private List<Klass> teachKlassList = new ArrayList<>();

    public Teacher(Integer id, String name, Integer age) {
        super(id, name, age);
    }

    @Override
    public String introduce() {
        String introduceIfBelongsToKlass = "";
        if (!teachKlassList.isEmpty()) {
            List<String> teachKlassNumberStringList = this.teachKlassList.stream()
                    .map(Klass::getNumber)
                    .map(Object::toString)
                    .collect(Collectors.toList());
            introduceIfBelongsToKlass = " I teach Class " + String.join(", ", teachKlassNumberStringList) + ".";
        }
        return String.format("My name is %s. I am %d years old. I am a teacher.", this.name, this.age)
                + introduceIfBelongsToKlass;
    }

    public void assignTo(Klass klass) {
        this.teachKlassList.add(klass);
    }

    public Boolean belongsTo(Klass klass) {
        for (Klass teachKlass: teachKlassList) {
            if (teachKlass.equals(klass)) {
                return true;
            }
        }
        return false;
    }

    public Boolean isTeaching(Student student) {
        return belongsTo(student.getKlass());
    }
}
