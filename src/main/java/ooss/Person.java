package ooss;

public class Person{

    protected Integer id;

    protected String name;

    protected Integer age;

    public Person(Integer id, String name, Integer age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }

    public String introduce() {
        return String.format("My name is %s. I am %d years old.", name, age);
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }

        if (!(o instanceof Person)) {
            return false;
        }

        Person anotherPerson = (Person) o;
        return this.id.equals(anotherPerson.id);
    }

}
