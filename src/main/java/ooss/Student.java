package ooss;

public class Student extends Person {

    private Klass klass;

    public Student(Integer id, String name, Integer age) {
        super(id, name, age);
    }

    @Override
    public String introduce() {
        String introduceIfBelongsToKlass = "";

        if (this.klass != null) {
            if (this.klass.isLeader(this)) {
                introduceIfBelongsToKlass = String.format(" I am the leader of class %d.", this.klass.getNumber());
            } else {
                introduceIfBelongsToKlass = String.format(" I am in class %d.", this.klass.getNumber());
            }
        }
        return String.format("My name is %s. I am %d years old. I am a student.", this.name, this.age)
                + introduceIfBelongsToKlass;
    }

    public void join(Klass klass) {
        this.klass = klass;
    }

    public Boolean isIn(Klass klass) {
        if (this.klass == null) {
            return false;
        }
        return this.klass.getNumber() == klass.getNumber();
    }

    public Klass getKlass() {
        return this.klass;
    }
}
